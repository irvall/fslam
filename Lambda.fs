module Absyn

type lambda =
    | Var of string
    | Abs of string * lambda
    | App of lambda * lambda

let rec fmt = function
    | Var x -> x
    | Abs(x,l) -> 
        let rec curry a = function
        | Abs(x,l) -> curry (a+x) l 
        | lam      -> sprintf "λ%s.%s" a (fmt lam)
        curry x l
    | App(l1,l2) -> // TODO: Fix nesting of paranthesis
        let wrap l =
            match l with
            | Var _ -> sprintf "%s" (fmt l)
            | _     -> sprintf "(%s)" (fmt l)
        sprintf "%s %s" (wrap l1) (wrap l2)

let evalNum = 
    function
    | Abs("f", Abs("x", l)) ->
        let rec increment n = function
        | Var _     -> n
        | App(_,l)  -> increment (n+1) l
        | _         -> failwith "Invalid lambda: expecting App or Var"
        increment 0 l
    | _ -> failwith "Expecting number skeleton: Abs(\"f\", Abs(\"x\",_))"

let createNum n =
    let rec builder = function
    | 0,lam -> lam
    | n,lam -> builder (n-1, App(Var "f", lam))
    Abs("f", Abs("x", builder (n,Var "x")))

let I = Abs("x", Var "x") 
let K = Abs("a", Abs("b", Var "a"))
let M = Abs("f", App(Var "f", Var "f"))
let O = App(M, M)
let S = App(Abs("f", Abs("g", Abs ("x", App(Var "f", Var "x")))), App(Var "g", Var "x"))
// Y = \h.(\x.h (x x)) (\x.h (x x))
let Y = App(Abs("h", App(Abs("x", Var "h"), I)), App(Abs("x", Var "h"), I))

// Math
//add = \m.\n.\f.\x.m f (n f x)
let add = App(Abs("m", Abs("n", Abs("f", Abs("x", App(Var "m", Var "f"))))), App(App(Var "n", Var "f"), Var "x"))

// Testing curried printing
let curried = Abs("a", Abs("b", Abs("c", Var "b"))) |> fmt

let rec reduce = failwith "Not implemented"
 